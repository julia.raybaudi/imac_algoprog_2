#include <tp5.h>
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;


std::vector<string> TP5::names(
{
    "Yolo", "Anastasiya", "Clement", "Sirine", "Julien", "Sacha", "Leo", "Margot",
    "JoLeClodo", "Anais", "Jolan", "Marie", "Cindy", "Flavien", "Tanguy", "Audrey",
    "Mr.PeanutButter", "Bojack", "Mugiwara", "Sully", "Solem",
    "Leo", "Nils", "Vincent", "Paul", "Zoe", "Julien", "Matteo",
    "Fanny", "Jeanne", "Elo"
});

int HashTable::hash(std::string element)
{
    // use this->size() to get HashTable size

    int result = (int) element[0];
    return result % (int)this->size();
    //si result < (int)this->size(), cela renvoie result
}

void HashTable::insert(std::string element)
{
    // use (*this)[i] or this->get(i) to get a value at index i

    int indice = hash(element);
    (*this)[indice] = element;
}

/**
 * @brief buildHashTable: fill the HashTable with given names
 * @param table table to fiil
 * @param names array of names to insert
 * @param namesCount size of names array
 */
void buildHashTable(HashTable& table, std::string* names, int namesCount)
{
}

bool HashTable::contains(std::string element)
{
    // Note: Do not use iteration (for, while, ...)
    
    int indice = hash(element);
    return ((*this)[indice] == element);
}

//Fonctions avec listes
class MyHashTable {
  std::vector<std::vector<string>> _data;
public:
  MyHashMap(int size=256) {
    _data.resize(256);
  }

  int hash(const string& str) {
    int result = (int) str[0];
    return result % _data.size();
    //si result < _data.size(), cela renvoie result
  }

  void insert(string str) {
    if(contains(str)){
        return;
    }
    _data[hash(str)].push_back(str);
  }

  bool contains(const string& str) {
    int hash = hash(str);
    for (int i = 0; i < _data[hash].size(); i++) {
        if(_data[hash][i] == str){
            return true;
        }
    }
    return false;
  }

};

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 10;
	w = new HashWindow();
	w->show();
	return a.exec();
}
#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    int taille;
    int capacite;
};

struct DynaTableau{
    int* donnees;
    int taille;
    int capacite;
};

//listes

void initialise(Liste* liste)
{
    liste->capacite = 16;
    liste->premier = NULL;
    liste->taille = 0;
}

bool est_vide(const Liste* liste)
{
    if(liste->taille == 0){
        return true;
    }
    return false;
}

void ajoute(Liste* liste, int valeur){

    if(liste->taille == liste->capacite){   //liste pleine
        int nouvelle_capacite;
        if(liste->capacite == 0){       //cas capacite nulle
            nouvelle_capacite = 16;
        }
        else{
            nouvelle_capacite = liste->capacite * 2;
        }
    }

    //on cherche le dernier noeud de la liste
    Noeud * dernier_noeud = liste->premier;

    while(dernier_noeud != NULL){
        dernier_noeud = dernier_noeud->suivant;
    }

    Noeud * nouveau_noeud;
    dernier_noeud->suivant = nouveau_noeud;
    nouveau_noeud->donnee = valeur;
    nouveau_noeud->suivant = NULL;  //c'est le dernier de la liste

    liste->taille ++;
}

void affiche(const Liste* liste)
{
    for(int i=0 ; i<liste->taille ; i++){
        cout<<"valeur "<<i<<" : "<<liste->premier->donnee<<"\n";
        liste->premier = liste->premier->suivant;
    }
}

int recupere(const Liste* liste, int n)
{
    if(n>=0 && n<=liste->taille){
        Noeud * noeud_actuel = liste->premier;
        for(int i=0 ; i<n-1 ; i++){
            noeud_actuel = noeud_actuel->suivant;
        }
        return noeud_actuel->donnee;
    }
    cout<<"On est en dehors de la liste.\n";
    return 0;
}

int cherche(const Liste* liste, int valeur)
{
    for(int i=0 ; i<liste->taille ; i++){
        if(liste->premier->donnee == valeur){
            return i;
        }
        else{
            liste->premier = liste->premier->suivant;
        }
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    if(n>=0 && n<=liste->nbNoeud){
        Noeud * noeud_actuel = liste->premier;
        for(int i=0 ; i<n-1 ; i++){
            noeud_actuel = noeud_actuel->suivant;
        }
        noeud_actuel->donnee = valeur;
    }
    else{
        cout<<"On est en dehors de la liste.\n";
    }
}

//tableaux

void ajoute(DynaTableau* tableau, int valeur)
{
    if(tableau->taille == tableau->capacite){   //tableau plein
        int nouvelle_capacite;
        if(tableau->capacite == 0){       //cas capacite nulle
            nouvelle_capacite = 16;
        }
        else{
            nouvelle_capacite = tableau->capacite * 2;
        }
    }

    tableau->donnees[tableau->taille]=valeur;

    tableau->taille ++;
}

void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite = capacite;
    tableau->donnees = int[capacite];
    tableau->taille = 0;

}

bool est_vide(const DynaTableau* tableau)
{
    if(tableau->taille ==0){
        return true;
    }
    return false;
}

void affiche(const DynaTableau* tableau)
{
    for(int i=0 ; i<tableau->taille ; i++){
        cout<<"valeur "<<i<<" : "<<tableau->donnees[i]<<"\n";
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if(n>=0 && n<=tableau->taille){
        return tableau->donnees[n];
    }
    cout<<"On est en dehors du tableau.\n"
    return 0;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for(int i=0 ; i<tableau->taille ; i++){
        if(tableau->donnees[i] == valeur){
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if(n>=0 && n<=liste->nbNoeud){
        tableau->donnees[n] = valeur;
    }
    else{
        cout<<"On est en dehors de la liste.\n";
    }
}


//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    //ajouter une valeur à la fin de la liste est la fonction ajoute(liste, valeur)
    //pour ajouter au début, il faut tout décaler
    Noeud * ancien_premier = liste->premier;
    liste->premier = liste->premier->suivant;
    ancien_premier->donnee = valeur;
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    int valeur = liste->premier->donnee;
    liste->premier = liste->premier->suivant;
    liste->taille--;
    return valeur;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{

}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    return 0;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&pile))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&liste) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}

#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    float module_z = sqrt(z.x*z.x + z.y*z.y);

    if(n==0){
        return 0;
    }

    if(module_z <=2 && n>0){
        float reel = z.x;
        z.x = z.x*z.x - z.y*z.y + point.x;
        z.y = 2*reel*z.y + point.y;

        return isMandelbrot(z, n-1, point);
    }
    else{
        return 1;
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}




#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex*2 +1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex*2 +2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    int i = heapSize;
    this->get(i) = value;
    while(i>0 && this->get(i) > this->get((i-1)/2) ){
        this->swap(i, (i-1)/2);
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
    int i = 0;
    int i_max = nodeIndex;

    //test de taille du tas
    if(rightChild(i_max)<heapSize){
        if(this->get(rightChild(i_max)) > this->get(i_max)){
            i_max = rightChild(i_max);
        }
    }
    if(leftChild(i_max)<heapSize){
        if(this->get(leftChild(i_max)) > this->get(i_max)){
            i_max = leftChild(i_max);
        }
    }

    if(i_max != nodeIndex){
        this->swap(nodeIndex, i_max);
        heapify(heapSize, i_max);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for(int i=0; i<numbers.size(); i++){
        insertHeapNode(i+1, numbers[i]);
    }
}

void Heap::heapSort()
{
    int n=this->size();
    for(int i=n-1 ; i>0 ; i--){
        this->swap(0,i);
        this->heapify(i,0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}

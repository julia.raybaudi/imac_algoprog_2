#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int value;

    void initNode(int value)
    {
        this->value=value;
        this->left=nullptr;
        this->right=nullptr;
    }

    uint max(uint a, uint b) {
        if (a > b) {
            return a;
        } 
        else {
            return b;
        }
    }

	void insertNumber(int value) {
        if (value <= this->value) {
            if (this->left == nullptr) {
                this->left = createNode(value);
            }
            else {
                this->left->insertNumber(value);
            }
        }
        else {
            if (this->right == nullptr) {
                this->right = createNode(value);
            }
            else {
                this->right->insertNumber(value);
            }
        }
    }

    uint height() const {
        uint hl = 0;
        if (this->left != nullptr) {
            hl = this->left->height();
        }
        uint hr = 0;
        if (this->right != nullptr) {
            hl = this->right->height();
        }

        return 1 + max(hl, hr);
    }

    uint nodesCount() const {
        uint cl = 0;
        if (this->left != nullptr) {
            cl = this->left->nodesCount();
        }
        uint cr = 0;
        if (this->right != nullptr) {
            cr = this->right->nodesCount();
        }

        return 1 + cl + cr;
    }

	bool isLeaf() const {
        return (this->left == nullptr && this->right == nullptr);
        //return true si la condition est remplie, false sinon
	}

	void allLeaves(Node *leaves[], uint& leavesCount) {
        if (this->isLeaf()) {
            leaves[leavesCount] = this;
            leavesCount++;
        }
        else {
            if (this->left != nullptr) {
                this->left->allLeaves(leaves, leavesCount);
            }
            if (this->right != nullptr) {
                this->right->allLeaves(leaves, leavesCount);
            }
        }
    }

	void inorderTraversal(Node *nodes[], uint& nodesCount) {

        if (this->left != nullptr) {
            this->left->inorderTraversal(nodes, nodesCount); 
        }
        
        nodes[nodesCount] = this;
        nodesCount++;

        if (this->right != nullptr) {
            this->right->inorderTraversal(nodes, nodesCount);
        }
    }

    void preorderTraversal(Node *nodes[], uint& nodesCount) {

        nodes[nodesCount] = this;
        nodesCount++;

        if (this->left != nullptr) {
            this->left->preorderTraversal(nodes, nodesCount); 
        }

        if (this->right != nullptr) {
            this->right->preorderTraversal(nodes, nodesCount);
        }
    }

	void postorderTraversal(Node *nodes[], uint& nodesCount) {

        if (this->left != nullptr) {
            this->left->postorderTraversal(nodes, nodesCount); 
        }

        if (this->right != nullptr) {
            this->right->postorderTraversal(nodes, nodesCount);
        }

        nodes[nodesCount] = this;
        nodesCount++;
    }

	find(int value) {
        if (value == this->value) {
            return this;
        }

        if (value < this->value && this->left != nullptr) {
            return this->left->find(value);
        }

        if (value > this->value && this->right != nullptr) {
            return this->right->find(value);
        }

        return nullptr;
    }

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}

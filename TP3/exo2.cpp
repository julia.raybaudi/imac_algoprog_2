#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <iostream>
using namespace std;

MainWindow* w = nullptr;
using std::size_t;


void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{
    indexMin = indexMax = -1;

    int start=0, end=array.size();

    //même principe que pour binarySearch, on cherche d'abord une apparition de toSearch
    while(start<end){
        int mid = (start+end)/2;

        if(toSearch>array[mid]){
            start = mid +1;
        }
        else if(toSearch<array[mid]){
            end = mid;
        }
        else{
            indexMin = mid;
            indexMax = mid;
        }
    }

    //Continuer à chercher les autres apparitions de toSearch
    
    while(array[indexMin] == array[mid]){
        mid = (start+mid)/2;
    }
    while(array[indexMin] > array[mid]){
        mid = (mid+indexMin)/2;
    }
    indexMin=mid;

    while(array[indexMax] == array[mid]){
        mid = (mid+end)/2;
    }
    while(array[indexMax] < array[mid]){
        mid = (mid+indexMax)/2;
    }
    indexMax=mid;

    //affichage des résultats
    if(indexMin==indexMax){
        if(indexMin==-1){
            cout<<"La valeur n'apparait pas dans le tableau.\n";
        }
        else{
            cout<<"La valeur n'apparait qu'en "<<indexMin<<".\n";
        }
    }
    else{
        cout<<"La premiere occurence est en "<<indexMin<<", et la derniere est en "<<indexMax<<".\n";
    }

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}

#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	int pas_trie = 1;							//booléen pour savoir si le tableau est trié

	while(pas_trie){
		pas_trie = 0;							//le tableau est trié
		
		for(int i=0 ; i<toSort.size-1 ; i++){
			if(toSort[i]>toSort[i+1]){
				toSort.swap(i,i+1);
				pas_trie = 1;
			}
		}

	}
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}

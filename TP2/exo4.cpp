#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int inf, int sup) {

    int size = sup - inf;

    //condition d'arrêt
    if (size <= 1) {
        return;
    }

    int p = inf;        //place du pivot
    int i = sup - 1;    //place du curseur

    while (i != p) {
        if (toSort[p] > toSort[i]) {
            toSort.swap(i, p+1);
            toSort.swap(p, p+1);
            p++;
        }
        else {
            i--;
        }
    }

    recursivQuickSort(toSort, inf, p);
    recursivQuickSort(toSort, p + 1, sup);
}

void quicksort(Array& toSort) {
 	recursivQuickSort(toSort, 0, toSort.size());
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}

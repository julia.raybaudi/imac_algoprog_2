#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
        // stop statement = condition + return (return stop the function even if it does not return anything)
    if (origin.size() <= 1) {
        return;
    }

        // initialisation
    int fsize = origin.size() / 2;
    int ssize = origin.size() - fsize;
    Array& first = w->newArray(fsize);
    Array& second = w->newArray(ssize);
        
        // split
    for (int i = 0; i < fsize; i++) {
        first.push_back(origin[i]);
    }

    for (int i = 0; i < ssize; i++) {
        second.push_back(origin[i + fsize]);
    }

        // recursiv splitAndMerge of lowerArray and greaterArray
    splitAndMerge(first);
    splitAndMerge(second);

        // merge
    merge(first, second, origin);
}

void merge(Array& first, Array& second, Array& result)
{
	// merge both arrays
	int i = 0, j = 0;
	while (i < first.size() && j < first.size()) {
		int a = first[i];
		int b = second[j];
		if (a < b) {
			result.push_back(a);
			i++;
		}
		else {
			result.push_back(b);
			j++;
		}
	}

	// merge remaining elements
	if (i == first.size()) { // first vide
		while (j < second.size()) {
			result.push_back(second.get(j));
			j++;
		}
	}
	else { // second vide
		while (i < first.size()) {
			result.push_back(first.get(i));
			i++;
		}
	}
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
